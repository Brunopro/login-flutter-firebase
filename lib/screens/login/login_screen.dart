import 'package:flutter/material.dart';
import 'widgets/form_container.dart';
import 'widgets/stagger_animation.dart';

import 'package:flutter/scheduler.dart' show timeDilation;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with SingleTickerProviderStateMixin {

  AnimationController _animationController;


  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
        vsync: this,
        duration: Duration(seconds: 10),
    );
  }


  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 1;
    return Scaffold(
      body: Body(),
    );
  }

  Widget Body(){
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage("images/background.jpg"),
        fit: BoxFit.cover
        )
      ),
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 70, bottom: 32),
                    child: Image.asset("images/tickicon.png",
                    width: 150.0,
                    height: 150.0,
                    fit: BoxFit.contain,
                    ),
                  ),
                  FormContainer(),
                ],
              ),
              StaggerAnimation(
                controller: _animationController.view
              )
            ],
          )
        ],
      ),
    );
  }

}
