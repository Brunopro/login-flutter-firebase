import 'package:flutter/material.dart';
import 'package:flutter_login_layout/screens/login/widgets/input_field.dart';

import 'sign_up_button.dart';



class FormContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        child: Column(
          children: <Widget>[
            InputField(
              hint: "Username",
              obscure: false,
              icon: Icons.people
            ),
            InputField(
              hint: "Password",
              obscure: true,
              icon: Icons.lock_outline
            ),
            SignUpButton()
          ],
        ),
      ),
    );
  }
}
